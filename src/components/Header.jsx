import React from "react"
import { Link, useNavigate } from "react-router-dom"
import { useUser } from "../store/UserContext"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { storageClear } from "../utils/storage"

const Header = () => {
  const { user, setUser } = useUser()
  const navigate = useNavigate()

  const onLogout = () => {
    storageClear(STORAGE_KEY_USER)
    setUser(null)
    navigate('')
  }
  
  return (
    <>
        { !user &&
        <div className="Header">
          <div className="HeaderLeft">
            <h2>Lost in Translation</h2>
          </div>
        <div className="HeaderRight"></div>
        </div>
        }

        { user &&
        <div className="Header">
          <div className="HeaderLeft">
            <img className="HeaderLogo" src="Logo.png" alt="droid"></img>
            <h2>Lost in Translation</h2>
          </div>
          <div className="HeaderRight">
            <Link className="TextLink" to="/translation">
              <button className="NavButton">Translations</button>
            </Link>
            <Link className="TextLink" to="/profile">
              <button className="NavButton">Profile</button>
            </Link>
            <button className="NavButton" onClick={ onLogout }>Logout</button>
          </div>
        </div>
        }
    </>
  )
}

export default Header