import React from "react"
import { clearTranslationHistory } from "../../api/user"
import { useUser } from "../../store/UserContext"
import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"

const ProfileActions = ({ userId }) => {
    const { setUser } = useUser()

    const onClearHistory = async () => {
        const [ error, userResponse ] = await clearTranslationHistory(userId)

        if (error !== null) {
            console.log(error)
        }
        
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
    }

    return (
        <ul className="ClearButtonStyle">
            <li><button className="ClearButton" onClick={ onClearHistory }>Clear history</button></li>
        </ul>
    )
}

export default ProfileActions