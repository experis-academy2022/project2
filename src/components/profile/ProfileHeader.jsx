import React from "react"

const ProfileHeader = ({ username }) => {
  return (
    <header className="ProfileHeader">
      <h3>Hello { username }, welcome back!</h3>
    </header>
  )
}

export default ProfileHeader