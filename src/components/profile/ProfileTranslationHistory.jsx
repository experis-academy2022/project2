import React from "react"
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
import ProfileActions from "./ProfileActions";
import { useUser } from "../../store/UserContext";


const ProfileTranslationHistory = ({ translations }) => {
  if (translations.length > 10) {
    translations = translations.slice(-10);
  }

  const { user } = useUser()
  let counter = 0

  const translationList = translations.map(translation =>
    <ProfileTranslationHistoryItem key={ translation + (counter += 1) } translation={ translation }/>)

  return (
    <section>
      <h3 className="Title">Your translation history</h3>
      <ul className="ProfileTranslationHistory">
        <div className="ProfileTranslationHistoryList">
          { translationList }
        </div>
      </ul>
      <footer><ProfileActions userId={ user.id } /></footer>
    </section>
  )
}

export default ProfileTranslationHistory