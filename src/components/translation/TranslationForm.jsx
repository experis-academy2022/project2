import React from "react"
import { useForm } from "react-hook-form"
import { useTranslation } from "../../store/TranslationContext"
import { useUser } from "../../store/UserContext"
import { storageSave } from "../../utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { setTranslations } from "../../api/user"

const TranslationForm = () => {
  const { register, handleSubmit } = useForm()
  const { setTranslationResult } = useTranslation()
  const { user, setUser } = useUser()

  const onSubmit = async data => {
    const translation = data.translationText

    setTranslationResult(translation)

    const [ error, userResponse ] = await setTranslations(user.id, [...user.translations, translation])

    if (error !== null) {
      console.log(error)
    }

    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
  }

  return (
    <div className="TranslationForm">
      <form onSubmit={ handleSubmit(onSubmit) }>
        <input type="text" {...register('translationText')} placeholder = "write something :)" className="TranslationInput"></input>
        <input type="submit" className="TranslationButton" />
        <img src="Arrow.png" alt="arrow" className="TranslationButtonIcon" />
      </form>
    </div>
  )
}

export default TranslationForm