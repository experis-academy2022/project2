import React from "react"
import { useTranslation } from "../../store/TranslationContext"

const TranslationResult = () => {
  const { translationResult } = useTranslation()
  const invalidCharactersRegex = /[^a-zA-Z]/g
  let counter = 0

  const returnSigns = translationResult.replace(invalidCharactersRegex, '')
    .toLowerCase()
    .split('')
    .map(char => {
      return <img src={`signs/${char}.png`} key={counter +=1} alt="sign" className="TranslationSign" />
    })

  return (
    <>
    <div className="ResultContainer">
      <div className="ResultInput">
        { returnSigns }
      </div>
    </div>
    </>
  )
}

export default TranslationResult