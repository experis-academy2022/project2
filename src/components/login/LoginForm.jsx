import React from "react"
import { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import { loginUser } from "../../api/user"
import { useUser } from "../../store/UserContext"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { storageSave } from "../../utils/storage"

const usernameConfig = {
  required: true,
  minLength: 3
}

const LoginForm = () => {
  // Hooks
  const { register, handleSubmit, formState: { errors } } = useForm()
  const { user, setUser } = useUser()
  const navigate = useNavigate()

  // Local State
  const [ apiError, setApiError ] = useState(null)

  // Side Effects
  useEffect(() => {
    if (user !== null) {
      navigate('Translation')
    }
    }, [ user, navigate ])
  
  // Event Handlers
  const onSubmit = async ({ username }) => {
    const [ error, userResponse ] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
  }

  // Render Functions
  const errorMessage = (() => {
    if (!errors.username) {
      return null
    }

    if (errors.username.type === 'required') {
      return <span className="FormError">Username is required</span>
    }

    if (errors.username.type === 'minLength') {
      return <span className="FormError">Username is too short (min 3 characters)</span>
    }
  })()

  return (
    <div className="LoginContainer">
      <div className="LoginForm">
        <form onSubmit={ handleSubmit(onSubmit) }>
          <input type="text" { ...register("username", usernameConfig) } placeholder="What's your name?" className="TextInput" />
          <input type="submit" className="SubmitButton" />
        </form>
        <img src="Arrow.png" alt="arrow" className="SubmitIcon" />
          { errorMessage }
          { apiError && <p>{ apiError }</p> }
      </div>
    </div>
  )
}

export default LoginForm