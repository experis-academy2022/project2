import React from "react"

const LoginBanner = () => {
  return (
    <>
      <div className="LoginBanner">
        <div className="LoginBannerLeft">
            <img className="LoginBannerLogo" src="Logo.png" alt="droid"></img>
        </div>
        <div className="LoginBannerRight">
          <h1>Lost in Translation</h1>
          <h3>Get Started</h3>
        </div>
      </div>
    </>
  )
}

export default LoginBanner