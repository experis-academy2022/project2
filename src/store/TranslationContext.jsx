import React from "react"
import { createContext, useContext, useState } from "react"

// Context -> exposing the value
const TranslationContext = createContext()

export const useTranslation = () => {
  return useContext(TranslationContext)
}

// Provide -> managing state
const TranslationProvider = ({ children }) => {
  const [ translationResult, setTranslationResult ] = useState("")

  const state = {
    translationResult, setTranslationResult
  }

  return (
    <TranslationContext.Provider value={ state }>
      { children }
    </TranslationContext.Provider>
  )
}

export default TranslationProvider