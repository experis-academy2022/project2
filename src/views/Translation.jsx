import React from "react"
import "./../styles/Translation.css"
import TranslationForm from "../components/translation/TranslationForm"
import TranslationResult from "../components/translation/TranslationResult"
import withAuth from "../hoc/withAuth"
import TranslationProvider from "../store/TranslationContext"

const Translation = () => {
  return (
    <>
      <TranslationProvider>
        <div className="ProfileHeaderContainer">
          <TranslationForm />
        </div>
        <div>
          <TranslationResult />
        </div>
      </TranslationProvider>
    </>
  )
}

export default withAuth(Translation)