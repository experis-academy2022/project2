import React from "react"
import "./../styles/Profile.css"
import ProfileHeader from "../components/profile/ProfileHeader"
import ProfileTranslationHistory from "../components/profile/ProfileTranslationHistory"
import withAuth from "../hoc/withAuth"
import { useUser } from "../store/UserContext"

const Profile = () => {

  const { user } = useUser()

  return (
    <>
      <div className="ProfileHeaderContainer">
        <ProfileHeader username={ user.username } />
      </div>
        <ProfileTranslationHistory translations={ user.translations } />
    </>
  )
}

export default withAuth(Profile)