import React from "react"
import "./../styles/Login.css"
import LoginBanner from "../components/login/LoginBanner"
import LoginForm from "../components/login/LoginForm"

const Login = () => {
  return (
    <>
      <LoginBanner />
      <LoginForm />
    </>
  )
}

export default Login