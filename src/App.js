import "./styles/App.css"
import React from "react"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Login from "./views/Login"
import Translation from "./views/Translation"
import Profile from "./views/Profile"
import Header from "./components/Header"


function App() {

  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Routes>
          <Route path="/" element={ <Login />}/>
          <Route path="/translation" element={ <Translation />}/>
          <Route path="/profile" element={ <Profile />}/>
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
