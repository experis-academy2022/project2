# Lost in translation

Lost in translation is an app which translates English words to American sign language. <br />
This is our second exercise at Experis Academy. We learnt a lot about SPA with React. <br />
App can also be viewed in Heroku: https://balder-gunnar-3.herokuapp.com/

<br>



## Features

The user is able to:

* Create an account by determining a username
* Login/Logout
* Translate letters, words & sentences to American sign language
* See 10 of one's latest translations
* Clear the translation history



<br>



## Installation

* To run the code on local machine you will need access to the API (.env file) or create your own backend.
* You will also need Node.js: https://nodejs.org/en/download/ and git: https://git-scm.com/downloads
* In your console navigate to the folder where you want to install the project and run the following commands:

```bash
git clone https://gitlab.com/experis-academy2022/project2.git
```
```bash
cd translations
```
```bash
npm install 
```

* For API access, create ".env" file in the root folder of the project.
* Add the following lines and paste the correct API key and url inside the quotation marks:
```bash
{    
    REACT_APP_API_KEY = "key"
    REACT_APP_API_URL = "base url"
}

```

<br>



## Usage

- You're able to run the App in the development mode by typing "npm start" in the command line after installation. <br /> Open http://localhost:3000 to view it in the browser.
- Enter a username to first create an account. Username must be at least 3 characters long.
- After logging in you will be directed to a Translation page where one can translate English words to sign language. <br /> Sign language will be shown in pictures.
- From the top left corner you can get to the Profile page where translation history will be shown. <br />
One is able to clear it, go back to Translation page or logout.

<br>

## Contributors
- Laura Koivuranta  https://gitlab.com/LauKoiFish
- Marie Puhakka     https://gitlab.com/mariesusan


<br>

## License
[MIT](https://choosealicense.com/licenses/mit/)
